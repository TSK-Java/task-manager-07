package ru.tsc.kirillov.tm.model;

import ru.tsc.kirillov.tm.constant.ArgumentConst;
import ru.tsc.kirillov.tm.constant.TerminalConst;

public class Command {

    public static Command ABOUT = new Command(
            TerminalConst.ABOUT,
            ArgumentConst.ABOUT,
            "Отображение информации о разработчике."
    );

    public static Command VERSION = new Command(
            TerminalConst.VERSION,
            ArgumentConst.VERSION,
            "Отображение версии программы."
    );

    public static Command HELP = new Command(
            TerminalConst.HELP,
            ArgumentConst.HELP,
            "Отображение доступных команд."
    );

    public static Command EXIT = new Command(
            TerminalConst.EXIT,
            null,
            "Закрытие приложения."
    );

    public static Command INFO = new Command(
            TerminalConst.INFO,
            ArgumentConst.INFO,
            "Отображение информации о системе."
    );

    private String name = "";

    private String argument = "";

    private String description = "";

    public Command() {}

    public Command(final String name) {
        this.name = name;
    }

    public Command(final String name, final String argument) {
        this.name = name;
        this.argument = argument;
    }

    public Command(final String name, String argument, final String description) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        String result = "";

        if (name != null && !name.isEmpty())
            result += name;
        if (argument != null && !argument.isEmpty())
            result += ", " + argument;
        if (description != null && !description.isEmpty())
            result += " - " + description;

        if (!result.isEmpty())
            return result;
        else
            return super.toString();
    }

}
